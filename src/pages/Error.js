import Banner from "../components/Banner";


function Error() {
  return <Banner origin="error"></Banner>;
}

export default Error;
