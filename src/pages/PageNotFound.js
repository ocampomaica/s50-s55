import {Row, Col, Button, Stack} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PageNotFound(){
	return (
        <Stack gap={2} className="col-md-5 mx-auto">
            <Row>
                <Col className = "text-center py-5">
                
                <img src="https://media4.giphy.com/media/C21GGDOpKT6Z4VuXyn/giphy.gif?cid=ecf05e47tyja68bfvnuqthp3ty8mgdr10i17077vugx839tq&rid=giphy.gif&ct=g" width="100%"/>
                    <Button className='mt-4' variant="success" as={ Link } to='/' >Return to Home Page</Button>
                </Col>
            </Row>
        </Stack>    
	)
} 
