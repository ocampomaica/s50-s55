import {Row, Col, Card} from 'react-bootstrap';

function Highlights(){
    return (
        <Row>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Learn From Home</Card.Title>
                        <Card.Text>
                            Learn coding while in the comfort of your home
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Study Now, Pay Later</Card.Title>
                        <Card.Text>
                           Finish the bootcamp first, pay Later
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Be Part of Our Community</Card.Title>
                        <Card.Text>
                           Apply as an instructor and share your knowledge
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}

export default Highlights;
