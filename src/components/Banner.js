import { Row, Col, Button } from "react-bootstrap";
import PageNotFound from './404';
function Banner(props) {
    const origin = props.origin;
  return (
    <>
        { origin === 'error' ? <PageNotFound/> :
            <Row>
            <Col className="p-5">
              <h1>Zuitt Coding Bootcamp</h1>
              <p>Opportunities for everyone, everywhere.</p>
              <Button variant="primary">Enroll Now!</Button>
            </Col>
          </Row>
    
        }    
    </>
     
      
   
  );
}

export default Banner;


